/*
 * TCombineDepth.cpp
 *
 *  Created on: Dec 10, 2017
 *      Author: phaentu
 */

#include "TCombineDepth.h"

//------------------------------------
//TInputFile
//------------------------------------
TInputFile::TInputFile(){
	filename = "";
	_open = false;
	value = 0.0;
	lineNum = 0;
	file = NULL;
}

TInputFile::TInputFile(std::string Filename){
	open(Filename);
}

void TInputFile::open(std::string Filename){
	filename = Filename;
	file = new gz::igzstream(Filename.c_str());
	if(file->eof())
		throw "Failed to open file '" + filename + "' for reading!";
	_open = true;

	//read header
	fillVectorFromLineWhiteSpaceSkipEmpty(*file, header);
	value = 0.0;
	lineNum = 0;
}

void TInputFile::close(){
	if(_open){
		file->close();
		delete file;
		_open = false;
	}
}

bool TInputFile::readLine(){
	if(file->good() && !file->eof()){
		fillVectorFromLineWhiteSpaceSkipEmpty(*file, data);
		++lineNum;

		//if empty, return false
		if(data.size() == 0)
			return false;

		if(data.size() != 3)
			throw "Wrong number of columns in '" + filename + "' on line " + toString(lineNum) + "!";

		value = stringToIntCheck(data[2]);
		return true;
	}
	return false;
}

//------------------------------------
//TOutputFile
//------------------------------------
TOutputFile::TOutputFile(std::string Filename){
	filename = Filename;
	file.open(Filename.c_str());
	if(!file)
		throw "Failed to open file '" + filename + "' for writing!";
	_open = true;
}

void TOutputFile::close(){
	if(_open){
		file.close();
		_open = false;
	}
}

void TOutputFile::writeHeader(const std::vector<std::string> & Header){
	file << concatenateString(Header, "\t") << "\n";
}

void TOutputFile::writeData(const std::string & chr, const std::string & pos, const double & val){
	file << chr << '\t' << pos << '\t' << val << '\n';
};


//------------------------------------
//TInputFileVector
//------------------------------------
TInputFileVector::TInputFileVector(std::map<std::string, double> & FilesAndDepthMap, TLog* logfile){
	//create vector
	filesAndDepthMap = FilesAndDepthMap;
	numFiles = filesAndDepthMap.size();
	files = new TInputFile[numFiles];
	fileVectorCreated = true;

	//open file pointers
	logfile->startIndent("Will parse the following input files:");
	std::map<std::string, double>::iterator iter;
	unsigned int i = 0;
	for(i=0, iter = filesAndDepthMap.begin(); iter != filesAndDepthMap.end() && i<numFiles; ++iter, ++i){
		logfile->list(iter->first);
		files[i].open(iter->first);
	}
	logfile->endIndent();

	//check header
	for(i=1; i<numFiles; ++i){
		if(files[i].header != files[0].header)
			throw "Header of file '" + files[i].filename + "' does not match header of file '" + files[0].filename + "'!";
	}

	_isGood = true;
	lineNum = 0;
	tmp = 0.0;
}

TInputFileVector::~TInputFileVector(){
	if(fileVectorCreated){
		delete[] files;
	}
}

bool TInputFileVector::readNext(){
	for(unsigned int i=0; i<numFiles; ++i){
		_isGood &= files[i].readLine();
	}
	if(!_isGood)
		return false;

	++lineNum;

	//check if positions match

	for(unsigned int i=1; i<numFiles; ++i){
		if(files[i].data[0] != files[0].data[0] || files[i].data[1] != files[0].data[1])
			throw "Position mismatch between files '" + files[0].filename + "' and '" + files[i].filename + "' on line " + toString(lineNum);
	}

	return true;
}

double TInputFileVector::sum(){
	tmp = files[0].value;
	for(unsigned int i=1; i<numFiles; ++i){
		tmp += files[i].value;
	}
	return tmp;
}

double TInputFileVector::weightedSum(){
	tmp = (float)files[0].value / filesAndDepthMap.at(files[0].filename);
	for(unsigned int i=1; i<numFiles; ++i){
		tmp += files[i].value / filesAndDepthMap.at(files[i].filename);
	}
	return tmp;
}

//------------------------------------
//TCombineDepth
//------------------------------------
TCombineDepth::TCombineDepth(TLog* Logfile){
	logfile = Logfile;
}

void TCombineDepth::combineDepth(TParameters & params){
	//now read input file names
	std::vector<std::string> filenames;
	std::map<std::string, double> filesAndDepthMap;
	if(params.parameterExists("inputFile")){
		std::string filename = params.getParameterString("inputFile");
		logfile->listFlush("Reading input bed files and corresponding depths from file '" + filename + "' ...");
		std::ifstream file(filename.c_str());
		if(!file) throw "Failed to open file '" + filename + "!";

		//tmp variables
		int lineNum = 0;

		//parse file
		while(file.good() && !file.eof()){
			++lineNum;
			std::vector<std::string> vec;
			fillVectorFromLineWhiteSpaceSkipEmpty(file, vec);
			if(!vec.empty()){
				if(vec.size() != 2) throw "Wrong number of entries on line " + toString(lineNum) + " in file '" + filename + "'!";
				//add to map
				filesAndDepthMap.insert(std::pair<std::string, double>(vec[0], stringToDouble(vec[1])));
			}
		}
		logfile->done();

	} else if(params.parameterExists("input")){
		params.fillParameterIntoVector("input", filenames, ',', true);
		for(unsigned int i; i<filenames.size(); ++i){
			filesAndDepthMap.insert(std::pair<std::string, double>(filenames[i], 1.0/(float) filenames.size()));
		}
	}

	//open input files
	TInputFileVector input(filesAndDepthMap, logfile);

	//open output file
	std::string outName = params.getParameterString("output");
	logfile->list("Will write combined depth to '" + outName + "_sum.txt.gz' and '" + outName + "_weightedSum.txt.gz'.");
	TOutputFile outSum(outName + "_sum.txt.gz");

	outSum.writeHeader(input.header());
	TOutputFile outWeightedSum(outName + "_weightedSum.txt.gz");
	outWeightedSum.writeHeader(input.header());

	//now parse line by line and sum depth
	logfile->startIndent("Parsing files:");
	struct timeval start, end;
    gettimeofday(&start, NULL);
    float runtime;
    while(input.readNext()){
		double sum = input.sum();
		outSum.writeData(input.chr(), input.pos(), sum);

		double weightedSum = input.weightedSum();
		outWeightedSum.writeData(input.chr(), input.pos(), weightedSum);

		//report progress
		if(input.lineNum  % 100000 == 0){
			gettimeofday(&end, NULL);
			runtime = (end.tv_sec  - start.tv_sec)/60.0;
			logfile->list("Parsed " + toString(input.lineNum) + " lines in " + toString(runtime) + " min.");
		}
	}

    //report end
    gettimeofday(&end, NULL);
    runtime = (end.tv_sec  - start.tv_sec)/60.0;
    logfile->list("Parsed " + toString(input.lineNum) + " lines in " + toString(runtime) + " min.");
    logfile->list("Reached end of files!");
}

void TCombineDepth::writeDepthDist(long* depthCounts, std::string outFilename, int & maxDepth, int & maxDepthPlusOne){
	//write counts
	outFilename = outFilename + "Mb_depthCounts.txt";
	logfile->list("Writing distribution to '" + outFilename + "'");
	std::ofstream counts(outFilename.c_str());
	if(!counts)
		throw "Failed to open output file '" + outFilename + "'";

	//header
	counts << "depth\tcounts\n";
	for(int d=0; d<maxDepthPlusOne; ++d){
		counts << d << '\t' << depthCounts[d] << '\n';
	}
	counts << '>' << maxDepth << '\t' << depthCounts[maxDepthPlusOne] << '\n';
	counts.close();
}

void TCombineDepth::writeCumulDistAndQuantiles(long* depthCounts, std::string outFilename, int & maxDepth, int & totalSites){
	int size = maxDepth + 1; // need >maxCov bin

	//open files
	std::ofstream outputNormalized;
	std::ofstream outputQuantiles;

	std::string outputFileNameNormalized = outFilename + "_normalizedCumulativeDepthPerSite.txt";
	std::string outputFileNameQuantiles = outFilename + "_quantilesDepthPerSite.txt";

	logfile->list("Writing normalized cumulative depth distribution to '" + outputFileNameNormalized + "'");
	logfile->list("Writing quantiles of cumulative depth distribution to '" + outputFileNameQuantiles + "'");

	outputNormalized.open(outputFileNameNormalized.c_str());
	outputQuantiles.open(outputFileNameQuantiles.c_str());

	if(!outputNormalized) throw "Failed to open output file '" + outputFileNameNormalized + "'!";
	if(!outputQuantiles) throw "Failed to open output file '" + outputFileNameQuantiles + "'!";

	//normalized cumulative distribution and quantiles
	double cumul = 0.0;
	double norm = 0.0;
	float percentages[14] = {0.001, 0.005, 0.01, 0.025, 0.05, 0.2, 0.5, 0.8, 0.9, 0.95, 0.975, 0.99, 0.995, 0.999};
	int numberOfPercentages = 14;
	int quantiles[numberOfPercentages];
	std::fill_n(quantiles, numberOfPercentages, -1);
	for(int i=0; i<(size); ++i){
		cumul += (double) (depthCounts[i]);
		if(i > 0 && norm == 1.0) outputNormalized << i << "\t" << 0 << "\n";
		else {
			norm = cumul / totalSites;
			outputNormalized << i << "\t" << norm << "\n";
		}
		for(int p=0; p<numberOfPercentages; ++p){
			//if smallest quantiles = 0
			if(i == 0 && norm > percentages[p]) quantiles[p] = i;
			else if(quantiles[p] == -1 && norm >= percentages[p]){
				quantiles[p] = i;
			}
		}
		if(quantiles[numberOfPercentages] == -1 && i >= percentages[numberOfPercentages]) quantiles[numberOfPercentages] = i;
	}
	if(norm == 1.0) outputNormalized << ">" << maxDepth << "\t" << 0 << "\n";
	else {
		cumul += (double) depthCounts[size];
		norm = cumul / totalSites;
		outputNormalized << ">" << maxDepth << "\t" << norm << std::endl;
	}

	for(int p=0; p<numberOfPercentages; ++p){
		if(quantiles[p] == -1) quantiles[p] = maxDepth;
	}

	for(int i=0; i < numberOfPercentages; ++i){
		outputQuantiles << percentages[i] << "\t" << quantiles[i] << std::endl;
	}

//	//clean up
//	if(nCharOnLine > 0){
//		output << '\n';
//		outputNormalized << '\n';
//	}
	outputNormalized.close();
	outputQuantiles.close();
}

void TCombineDepth::getDepthDist(TParameters & params){
	//prepare binning
	int maxDepth = params.getParameterIntWithDefault("maxDepth", 500);
	int maxDepthPlusOne = maxDepth + 1;
	int maxDepthPlusTwo = maxDepth + 2;

	logfile->list("Will asses depth distribution up to a depth of " + toString(maxDepthPlusOne));

	//create depth bins
	long* depthCounts = new long[maxDepthPlusTwo];
	for(int d=0; d<maxDepthPlusTwo; ++d){
		depthCounts[d] = 0;
	}

	//open input file
	std::string filename = params.getParameterString("input");
	logfile->startIndent("Parsing file '" + filename + "':");
	TInputFile input(filename);

	//output file
	std::string outFilename = params.getParameterStringWithDefault("output", "depthCounts.txt");

	//other variables
	struct timeval start, end;
    gettimeofday(&start, NULL);
    float runtime;

	//now parse
	int counter = 0;
	logfile->startIndent("Parsing file:");
	while(input.readLine()){
		++counter;
		if(input.value > maxDepth)
			++depthCounts[maxDepthPlusOne];
		else
			++depthCounts[input.value];
		if(counter % 100000000 == 0){
			std::string tmpOutFileName = outFilename + "_" + toString((float)counter/1000000.0) + "Mb";
			writeDepthDist(depthCounts, tmpOutFileName, maxDepth, maxDepthPlusOne);
			writeCumulDistAndQuantiles(depthCounts, tmpOutFileName, maxDepth, counter);
			logfile->list("Parsed " + toString(input.lineNum) + " lines in " + toString(runtime) + " min.");
		}
	}
	std::string tmpOutFileName = outFilename + "_total";
	writeDepthDist(depthCounts, tmpOutFileName, maxDepth, maxDepthPlusOne);
	writeCumulDistAndQuantiles(depthCounts, tmpOutFileName, maxDepth, counter);

    //report end
    gettimeofday(&end, NULL);
    runtime = (end.tv_sec  - start.tv_sec)/60.0;
    logfile->list("Parsed " + toString(input.lineNum) + " lines in " + toString(runtime) + " min.");
    logfile->list("Reached end of files!");
}

void TCombineDepth::writeMask(TParameters & params){
	//open input file
	std::string filename = params.getParameterString("input");
	logfile->startIndent("Parsing file '" + filename + "':");
	TInputFile input(filename);

	//output file
	std::string outFilename = params.getParameterStringWithDefault("output", "mask.bed");
	logfile->list("Writing distribution to '" + outFilename + "'");
	gz::ogzstream bed(outFilename.c_str());
	if(!bed)
		throw "Failed to open output file '" + outFilename + "'";

	//header
	bed << "chr\tbeginning\tend\n";

	//other variables
	double maxDepth = params.getParameterDouble("maxDepth");
	struct timeval start, end;
    gettimeofday(&start, NULL);
    float runtime;

	//now parse
	int counter = 0;
	logfile->startIndent("Parsing file:");
	while(input.readLine()){
		++counter;
		if(stringToDouble(input.data[2]) > maxDepth)
			bed << input.data[0] << "\t" << input.data[1] << "\t" << stringToIntCheck(input.data[1]) + 1 << "\n";
		if(counter % 100000000 == 0){
			gettimeofday(&end, NULL);
			runtime = (end.tv_sec  - start.tv_sec)/60.0;
			logfile->list("Parsed " + toString(input.lineNum) + " lines in " + toString(runtime) + " min.");
		}
	}
    //report end
    gettimeofday(&end, NULL);
    runtime = (end.tv_sec  - start.tv_sec)/60.0;
    logfile->list("Parsed " + toString(input.lineNum) + " lines in " + toString(runtime) + " min.");
    logfile->list("Reached end of files!");
}
