/*
 * combineDepth.cpp
 *
 *  Created on: Dec 10, 2017
 *      Author: phaentu
 */


#include "TParameters.h"
#include "stringFunctions.h"
#include "gzstream.h"
#include "TCombineDepth.h"

//---------------------------------------------------------------------------
//Main function
//---------------------------------------------------------------------------
int main(int argc, char* argv[]){
	struct timeval start, end;
    gettimeofday(&start, NULL);

	TLog logfile;
	logfile.newLine();
	logfile.write(" combineDepth ");
	logfile.write("**************");
    try{
		//read parameters from the command line
    	TParameters myParameters(argc, argv, &logfile);
    	logfile.setVerbose(true);

    	 //run combine depth
    	TCombineDepth core(&logfile);

    	//switch task
    	std::string task = myParameters.getParameterStringWithDefault("task", "combine");
    	if(task == "combine"){
    		logfile.startIndent("Combining multiple depth files:");
    		core.combineDepth(myParameters);
    	} else if(task == "dist"){
    		logfile.startIndent("Assemble depth distribution:");
    		core.getDepthDist(myParameters);
    	} else if(task == "writeMask"){
    		logfile.startIndent("Make depth mask:");
    		core.writeMask(myParameters);
    	} else throw "Unknown task '" + task + "'!";
    	logfile.endIndent();

		//write unsused parameters
		std::string unusedParams=myParameters.getListOfUnusedParameters();
		if(unusedParams!=""){
			logfile.newLine();
			logfile.warning("The following parameters were not used: " + unusedParams + "!");
		}
    }
	catch (std::string & error){
		logfile.error(error);
	}
	catch (const char* error){
		logfile.error(error);
	}
	catch(std::exception & error){
		logfile.error(error.what());
	}
	catch (...){
		logfile.error("unhandeled error!");
	}
	logfile.clearIndent();
	gettimeofday(&end, NULL);
	float runtime=(end.tv_sec  - start.tv_sec)/60.0;
	logfile.list("Program terminated in ", runtime, " min!");
	logfile.close();

    return 0;
}


