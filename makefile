#make file for spaunge

SRC = $(wildcard *.cpp) 

OBJ = $(SRC:%.cpp=%.o)

BIN = combineDepth

all:	$(BIN)

$(BIN):	$(OBJ)
	$(CXX) -O3 -o $(BIN) $(OBJ) -lz

%.o: %.cpp
	$(CXX) -O3 -c -std=c++1y $< -o $@

clean:
	rm -rf $(BIN) $(OBJ)



