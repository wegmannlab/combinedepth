/*
 * TCombineDepth.h
 *
 *  Created on: Dec 10, 2017
 *      Author: phaentu
 */

#ifndef TCOMBINEDEPTH_H_
#define TCOMBINEDEPTH_H_

#include "TLog.h"
#include "stringFunctions.h"
#include <sys/time.h>
#include "gzstream.h"
#include "TParameters.h"

//------------------------------------
//TInputFile
//------------------------------------
class TInputFile{
private:
	gz::igzstream* file;
	std::ifstream inFile;
	bool _open;

public:
	std::string filename;
	std::vector<std::string> header;
	std::vector<std::string> data;
	int value;
	long lineNum;

	TInputFile();
	TInputFile(std::string Filename);
	~TInputFile(){
		close();
	};
	void open(std::string Filename);
	void close();
	bool readLine();
};

//------------------------------------
//TOutputFile
//------------------------------------
class TOutputFile{
private:
	gz::ogzstream file;
	std::string filename;
	bool _open;

public:
	TOutputFile(std::string Filename);
	~TOutputFile(){
		close();
	};
	void close();
	void writeHeader(const std::vector<std::string> & Header);
	void writeData(const std::string & chr, const std::string & pos, const double & val);
};

//------------------------------------
//TInputFileVector
//------------------------------------
class TInputFileVector{
private:
	TInputFile* files;
	std::map<std::string, double> filesAndDepthMap;
	size_t numFiles;
	bool fileVectorCreated;
	bool _isGood;

	double tmp;

public:
	long lineNum;

	TInputFileVector(std::map<std::string, double> & filenames, TLog* logfile);
	~TInputFileVector();

	bool readNext();
	const std::vector<std::string>& header(){return files[0].header;};
	const std::string& chr(){return files[0].data[0];};
	const std::string& pos(){return files[0].data[1];};
	double sum();
	double weightedSum();
};


//------------------------------------
//TCombineDepth
//------------------------------------
class TCombineDepth{
private:
	TLog* logfile;

public:
	TCombineDepth(TLog* Logfile);
	~TCombineDepth(){};

	void combineDepth(TParameters & params);
	void writeCumulDistAndQuantiles(long* depthCounts, std::string outFilename, int & maxDepth, int & totalSites);
	void writeDepthDist(long* depthCounts, std::string outFilename, int & maxDepth, int & maxDepthPlusOne);
	void getDepthDist(TParameters & params);
	void writeMask(TParameters & params);

};


#endif /* TCOMBINEDEPTH_H_ */
